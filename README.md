# Django Encrypted File Field

This is a small wrapper around Django's `FileField` that uses `cryptography.fernet` to encrypt and decrypt files read from an arbitrary storage. It's been tested with `FileSystemStorage`, but should (in theory) work with any storage.

> **NOTE**: This field must load the entire file into memory to encrypt it / decrypt. As such, it may be prudent to limit the maximum file size that can be uploaded. By default, Django enables 2 file upload handlers, one of which is the `TemporaryFileUploadHandler` which stores larger files on-disk. You can disable this by setting 

```
FILE_UPLOAD_HANDLERS = [
    'django.core.files.uploadhandler.MemoryFileUploadHandler'
]
```
in your `settings.py`

## Installation
The package isn't in PyPI, so you'll have to install it directly from the git repo:

```bash
pip install git+https://git.uwaterloo.ca/mvucicevich/django-encrypted-filefield.git
```

We use git tag versioning, so you should be able to install a specific version by appending `@<tag>` to the URL.

## Usage

```python
# models.py
from django.conf import settings
from encrypted_file_field import EncryptedFileField

class MyModel(models.Model):
    file = EncryptedFileField(
        upload_to='encrypted_uploads/',
        password_fn=(lambda instance: instance.password),
        salt_fn=(lambda _x: settings.SECRET_KEY),
        url_fn=(lambda instance: f'/download/{instance.pk}/'),
    )
    password = models.CharField(max_length=255)
```

If you're backing up the encrypted files with a copy of the database, it's recommended you back up the salt separately to prevent an attacker from decrypting the files with access to only the backups. 

If you need to rotate or change your password, you should re-encrypt any relevant files. The library supports this by providing a `re_encrypt` function on the field:

```python
for m in MyModel.objects.all():
    m.file.re_encrypt(password=new_password, salt=new_salt)  # Both optional
    m.save()
```
Note that this will decrypt using original `salt_fn` and `password_fn`, so if you derive these values from `instance`, it will be best to run the update before changing `instance`. 

If you need to re-encrypt after the `instance` has already updated, you can use the `old_password` and `old_salt` arguments to `re_encrypt` instead.


The field does not add any signals to the model, so if you depend on one or more fields for salt or password, you'll need to add a signal or `save()` logic to the model to re-encrypt the file when changes occur.


## Helpers

To make it easy to download these files, we provide a shortcut function that can be used to serve the files:

```python
from encrypted_file_field.shortcuts import decrypt_and_serve_file

def download(request, pk):
    instance = get_object_or_404(MyModel, pk=pk)
    check_permissions(request, instance)
    return decrypt_and_serve_file(instance.file)
```

You'll need to implement your own logic for permission checks, etc.