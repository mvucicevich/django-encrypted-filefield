from django.db.models.fields.files import FileField, FieldFile, File
from .encrypted_file import EncryptedFile


class EncryptedFieldFile(FieldFile):
    @property
    def password(self):
        return self.field.password_fn(self.instance)
    
    @property
    def salt(self):
        return self.field.salt_fn(self.instance)

    @property
    def url(self):
        if self.field.url_fn:
            return self.field.url_fn(self.instance)
        return ''
    
    def open(self, mode="rb"):
        self._require_file()
        if getattr(self, "_file", None) is None:
            self._file = self.storage.open(self.name, mode)
        if not isinstance(self._file, EncryptedFile):
            self._file = EncryptedFile.openEncrypted(self._file, self.password, self.salt)
            print(self._file)
        if self._file.encrypted:
            self._file.decrypt()
        return self
    
    def save(self, name, content, save=True):
        name = self.field.generate_filename(self.instance, name)
        # if content.size > settings.ENCRYPTED_STORAGE_MAX_SIZE:
        #     raise Exception("File too large for encryption")
        content = EncryptedFile.openDecrypted(content, self.password, self.salt)
        self.name = self.storage.save(name, content)
        setattr(self.instance, self.field.name, self.name)
        self._file = None
        if save:
            self.instance.save()
    
    def re_encrypt(
        self,
        password=None,
        salt=None,
        old_password=None,
        old_salt=None,
        save=False
    ):
        f = EncryptedFile.openEncrypted(
            self.storage.open(self.name, 'rb'),
            old_password or self.password,
            old_salt or self.salt
        )
        f.update_cipher(
            password or self.password,
            salt or self.salt
        )
        f.encrypt()
        # Delete the old version of the file
        self.storage.delete(self.name)
        # Save the re-encrypted one
        self.storage.save(self.name, f)
        self._file = None
        if save:
            self.instance.save()

 


class EncryptedFileField(FileField):
    attr_class = EncryptedFieldFile

    def __init__(self, *args, password_fn=None, url_fn=None, salt_fn=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.password_fn = password_fn
        self.url_fn = url_fn
        self.salt_fn = salt_fn