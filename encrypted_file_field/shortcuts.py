from django.http import FileResponse
import mimetypes

def decrypt_and_serve_file(enc_file_field):
    # Given an EncryptedFieldFile, return a FileResponse with the unencrypted file.
    f = enc_file_field.open('rb')
    response = FileResponse(enc_file_field.open('rb'))
    response['Content-Type'] = mimetypes.guess_type(enc_file_field.name)[0] or 'application/octet-stream'
    response['Content-Disposition'] = f'filename="{enc_file_field.name}"'
    return response
