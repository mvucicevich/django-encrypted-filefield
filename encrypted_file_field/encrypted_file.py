from cryptography.fernet import Fernet
from django.core.files.base import File
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from io import BytesIO
import base64


def derive_key(password, salt, iterations=480000):
    # Derive us our key, as per the cryptography docs
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=salt.encode(),
        iterations=iterations,
    )
    return base64.urlsafe_b64encode(kdf.derive(password.encode()))


class EncryptedFile(File):
    """
    The bread and butter of this library. Basically called via 
    EncryptedFile.openEncrypted or EncryptedFile.openDecrypted, this class
    will read a file-like object, and wrap it in a BytesIO

    If necessary the object can be opened, written to, then re-encrypted

    All operations are done in memory, so this is not suitable for large files,
    and the file should be committed to a storage manually after writing to it
    """
    def __new__(cls, file_like, password, salt):
        self = super().__new__(cls)
        self.source = file_like
        self.name = file_like.name or "Encrypted File (no name)"
        self.file = None
        self.cipher = Fernet(derive_key(password, salt))
        return self
    
    def update_cipher(self, password, salt):
        re_encrypt = False
        if self.encrypted:
            re_encrypt = True
            self.decrypt()
        self.cipher = Fernet(derive_key(password, salt))
        
        # Return to the initial state
        if re_encrypt:
            self.encrypt()

    @classmethod
    def openEncrypted(cls, file_like, password, salt, decrypt=True):
        ob = cls.__new__(cls, file_like, password, salt)
        if decrypt:
            # Decrypt the file upon opening
            ob.file = BytesIO(ob.cipher.decrypt(ob.source.read()))
            ob.encrypted = False
        else:
            ob.file = BytesIO(ob.source.read())
            ob.ecrypted = True
        ob.file.seek(0)
        return ob

    @classmethod
    def openDecrypted(cls, file_like, password, salt, encrypt=True):
        ob = cls.__new__(cls, file_like, password, salt)
        if encrypt:
            # Encrypt the file upon opening
            ob.file = BytesIO(ob.cipher.encrypt(ob.source.read()))
            ob.encrypted = True
        else:
            ob.file = BytesIO(ob.source.read())
            ob.encrypted = False
        ob.file.seek(0)
        return ob
    
    def encrypt(self):
        if not self.encrypted:
            self.file.seek(0)
            self.file = BytesIO(self.cipher.encrypt(self.file.read()))
        self.file.seek(0)
        self.encrypted = True
    
    def decrypt(self):
        if self.ecrypted:
            self.file.seek(0)
            self.file = BytesIO(self.cipher.decrypt(self.file.read()))
        self.file.seek(0)
        self.encryped = False